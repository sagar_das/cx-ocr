import pytesseract
from pdf2image import convert_from_path

def detect_text_from_pdf(path):
    """Detects text in the file."""

    images = convert_from_path(path)

    for index,single_image in enumerate(images):

        print("------- PAGE "+str(index+1)+" ----------")
        text = pytesseract.image_to_string(single_image)
        print(text)
        
        

detect_text_from_pdf('/data/Data.pdf')