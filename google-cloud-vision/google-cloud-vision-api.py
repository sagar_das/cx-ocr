import os, io, base64
import csv
from google.cloud import vision
from pdf2image import convert_from_path, convert_from_bytes
import datefinder
import nltk
from nltk.tag import pos_tag
import pandas as pd
import pytesseract
from PIL import Image, ImageDraw

from enum import Enum

from pdf2image.exceptions import (
    PDFInfoNotInstalledError,
    PDFPageCountError,
    PDFSyntaxError
)
import cv2
import numpy as np
from pprint import pprint

field_name_dict = {'LEGAL BUSINESS NAME': None,
                   'PRIMARY CONTACT': None,
                   'PRIMARY PHONE': None,
                   'PRIMARY EMAIL': None,
                   'BILLING CONTACT PERSON': None,
                   'BILLING PHONE': None,
                   'BILLING EMAIL': None

                   }

date_data = {
    'page': [],
    'text': [],
    'date': [],
    'score': [],
}


class FeatureType(Enum):
    PAGE = 1
    BLOCK = 2
    PARA = 3
    WORD = 4
    SYMBOL = 5

file_path = "/Users/sagardas/Code/Python/cx-ocr/data/8006A000000ECNxQAO-Dedicated Nurse Educator Services Agreement.pdf"
file_name = os.path.basename(file_path)
pass


def draw_boxes(image, bounds, color):
    """Draw a border around the image using the hints in the vector list."""
    draw = ImageDraw.Draw(image)

    for bound in bounds:
        draw.polygon([
            bound.vertices[0].x, bound.vertices[0].y,
            bound.vertices[1].x, bound.vertices[1].y,
            bound.vertices[2].x, bound.vertices[2].y,
            bound.vertices[3].x, bound.vertices[3].y], None, color)
    return image


def get_document_bounds(image, feature, page_num):
    """Returns document bounds given an image."""
    client = vision.ImageAnnotatorClient()

    bounds = []

    response = client.document_text_detection(image=image)
    document = response.full_text_annotation

    print(u'Full text:\n{}'.format(
        document.text))

    paragraphs = []
    lines = []
    breaks = vision.enums.TextAnnotation.DetectedBreak.BreakType

    # Collect specified feature bounds by enumerating all document features
    for page in document.pages:
        for block in page.blocks:
            para = ""
            line = ""
            for paragraph in block.paragraphs:


                print('Paragraph confidence: {}'.format(
                    paragraph.confidence))

                for word in paragraph.words:

                    word_text = ''.join([
                        symbol.text for symbol in word.symbols
                    ])


                    for symbol in word.symbols:
                        if (feature == FeatureType.SYMBOL):
                            bounds.append(symbol.bounding_box)
                        line += symbol.text
                        if symbol.property.detected_break.type == breaks.SPACE:
                            line += ' '
                        if symbol.property.detected_break.type == breaks.EOL_SURE_SPACE:
                            line += ' '
                            lines.append(line)
                            para += line
                            line = ''
                        if symbol.property.detected_break.type == breaks.LINE_BREAK:
                            lines.append(line)
                            para += line
                            line = ''


                    if (feature == FeatureType.WORD):
                        bounds.append(word.bounding_box)

                paragraphs.append(para)
                # TODO: Extract dates along with their type
                # print("------Extracted Dates------")
                detect_dates_from_text(page_num, str(para), str(paragraph.confidence))




                if (feature == FeatureType.PARA):
                    bounds.append(paragraph.bounding_box)



            if (feature == FeatureType.BLOCK):
                bounds.append(block.bounding_box)

    # The list `bounds` contains the coordinates of the bounding boxes.

    #detect_dates_from_text(page_num, document.text, "")
    """Creating pandas data frame from date data"""
    write_date_data_to_csv()

    detect_proper_nouns_from_text(document.text)
    return bounds


def render_doc_text(image, pil_image, fileout, page_num):
    # bounds = get_document_bounds(image, FeatureType.BLOCK)
    # draw_boxes(pil_image, bounds, 'blue')
    bounds = get_document_bounds(image, FeatureType.PARA, page_num)
    draw_boxes(pil_image, bounds, 'red')
    # bounds = get_document_bounds(image, FeatureType.WORD)
    # draw_boxes(pil_image, bounds, 'yellow')

    if fileout != 0:
        pil_image.save(fileout)
    else:
        pil_image.show()


os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/Users/sagardas/Code/Python/cx-ocr/api-keys/vision-api.json'


def image_to_byte_array(image):
    imgByteArr = io.BytesIO()
    image.save(imgByteArr, format='PNG')
    imgByteArr = imgByteArr.getvalue()
    return imgByteArr


def write_to_csv():
    with open('ocr_results.csv', mode='w') as csv_file:
        fieldnames = [val for i, val in enumerate(field_name_dict.keys())]
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerow(field_name_dict)


def write_date_data_to_csv():
    df = pd.DataFrame(date_data, columns=['page', 'text', 'date', 'score'])
    df.to_csv('/Users/sagardas/Code/Python/cx-ocr/result/'+file_name+'.csv', index=False, header=True)


def detect_text_from_image(path):
    """Detects text in the file."""

    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()

    image = vision.types.Image(content=content)

    response = client.text_detection(image=image)
    texts = response.text_annotations
    print('Texts:')

    for text in texts:
        print('\n"{}"'.format(text.description))

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                     for vertex in text.bounding_poly.vertices])

        print('bounds: {}'.format(','.join(vertices)))

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))


def detect_text_from_pdf(path):
    """Detects text in the file."""

    client = vision.ImageAnnotatorClient()

    images = convert_from_path(path)

    for index, single_image in enumerate(images):

        print("------- PAGE " + str(index + 1) + " ----------")
        imgbytearr = image_to_byte_array(single_image)

        image = vision.types.Image(content=imgbytearr)

        response = client.text_detection(image=image)
        texts = response.text_annotations

        for text in texts:

            if 'Name:' not in text.description and "Title:" not in text.description:
                continue

            render_doc_text(image, single_image, '/Users/sagardas/Code/Python/cx-ocr/result/'+file_name+'.png', str(index + 1))

            # print('\n"{}"'.format(text.description))
            # tess_text = pytesseract.image_to_string(single_image)
            #
            # print("---Tess text-----")
            # print(tess_text)

            #
            #
            # field_values = text.description.split('\n')
            #
            # for key_value in field_name_dict.keys():
            #     indices = [i for i, x in enumerate(field_values) if key_value in x]
            #     field_name_dict[key_value] = field_values[indices[0]+1]
            #
            # pprint(field_name_dict)
            #
            # vertices = (['({},{})'.format(vertex.x, vertex.y)
            #             for vertex in text.bounding_poly.vertices])
            #
            # print('bounds: {}'.format(','.join(vertices)))
            #
            # single_image = np.array(single_image)
            # single_image = cv2.rectangle(single_image, (185,141), (1469,2088), (0,255,0), 2)
            # #resized = cv2.resize(single_image, (300,500), interpolation=cv2.INTER_AREA)
            # cv2.imwrite("data.jpg",single_image)
            # cv2.waitKey(0)
            #

            return

        if response.error.message:
            raise Exception(
                '{}\nFor more info on error messages, check: '
                'https://cloud.google.com/apis/design/errors'.format(
                    response.error.message))


def detect_text_from_base64(base64_data_raw):
    """Detects text in the file."""

    try:

        client = vision.ImageAnnotatorClient()

        base64_data = base64_data_raw.split(',')

        base64_in_bytes = base64_data[1].encode('ascii')
        data_in_bytes = base64.b64decode(base64_in_bytes)

        images = convert_from_bytes(data_in_bytes)

        for index, single_image in enumerate(images):

            imgbytearr = image_to_byte_array(single_image)

            image = vision.types.Image(content=imgbytearr)

            response = client.text_detection(image=image)
            texts = response.text_annotations

            for text in texts:

                if 'PROFILE SHEET' not in text.description:
                    continue

                field_values = text.description.split('\n')

                for key_value in field_name_dict.keys():
                    indices = [i for i, x in enumerate(field_values) if key_value in x]
                    field_name_dict[key_value] = field_values[indices[0] + 1]

                return (field_name_dict)

            if response.error.message:
                raise Exception(
                    '{}\nFor more info on error messages, check: '
                    'https://cloud.google.com/apis/design/errors'.format(
                        response.error.message))

    except Exception as e:
        return (str(e))


def detect_dates_from_text(page, text, score):
    text_arr = text.split("\n")

    for index, txt in enumerate(text_arr):
        matches = datefinder.find_dates(txt)
        try:
            for match in matches:
                try:
                    date_data['page'].append(page)
                    date_data['text'].append(text_arr[index - 1])
                    date_data['date'].append(str(match))
                    date_data['score'].append(score)
                    print(text_arr[index - 1])
                    print(match)
                except Exception as e:
                    continue
        except Exception as e:
            return


def detect_nouns_from_text(lines):
    tokenized = nltk.word_tokenize(lines)
    nouns = [word for (word, pos) in nltk.pos_tag(tokenized) if (pos[:2] == 'NN')]
    print("----NOUNS----")
    print(nouns)


def detect_proper_nouns_from_text(sentence):
    tagged_sent = pos_tag(sentence.split())
    propernouns = [word for word, pos in tagged_sent if pos == 'NNP']
    print("----Proper NOUNS----")
    print(propernouns)


# detect_text_from_base64("")


detect_text_from_pdf(file_path)
